package com.danylko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    public static Logger logger1 = LogManager.getLogger("com.danylko.console");

    public static void main(String[] args) {

        logger1.trace("This is a trace message");
        logger1.debug("This is a debug message");
        logger1.info("This is an info message");
        logger1.warn("This is a warn message");
        logger1.error("This is an error message");
        logger1.fatal("This is a fatal message");
        Test t = new Test();
        t.getMassage();
        Test2 t2 = new Test2();
        t2.getMassage();
        Test3 t3 = new Test3();
        t3.getMassage();
        Test4 t4 = new Test4();
        t4.getMassage();
        Test5 t5 = new Test5();
        t5.getMassage();

    }
}
