package com.danylko;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class Test2 {
    private Logger logger2 = LogManager.getLogger("com.danylko.Test2");


    public void getMassage() {
        logger2.trace("This is a trace message");
        logger2.debug("This is a debug message");
        logger2.info("This is an info message");
        logger2.warn("This is a warn message");
        logger2.error("This is an error message");
        logger2.fatal("This is a fatal message");

    }
}
